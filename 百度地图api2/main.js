// 百度地图API功能
var map = new BMap.Map("l-map");

var position = [110.751, 27.5379];
map.centerAndZoom(new BMap.Point(position[0], position[1]), 14); // 设置地图中心为止，以及缩放程度
map.enableScrollWheelZoom(true); //开启鼠标滚轮缩放

var driving = new BMap.DrivingRoute(map, {
    renderOptions: {
        map: map,
        panel: "r-result",
        autoViewport: false,
        selectFirstResult: true,
        highlightMode: BMAP_HIGHLIGHT_ROUTE
    }
}); // 自驾游路线

var transit = new BMap.TransitRoute(map, {
    renderOptions: {
        map: map,
        panel: "r-result2",
        autoViewport: false,
        selectFirstResult: true
    }
}); // 公交路线

var button = document.querySelector('button');
var keyword = document.querySelector('#keyword');
var transit1 = document.querySelector('#transit');
var driving1 = document.querySelector('#driving');
var a_s = document.querySelectorAll('.lst-d >a');
var result1 = document.querySelector('#r-result');
var result2 = document.querySelector('#r-result2');

var s_or_e = document.querySelector("#s-or-e");
// 起点或者终点
var re_color = function() {
    a_s.forEach(function(ele, index) {
        ele.style.color = 'black';
    });
}

var start_destination = '隆回';

button.onclick = function() {
    let v = keyword.value;
    let class_name = s_or_e.className;
    if (v != '') {
        transit.clearResults();
        driving.clearResults();
        if (class_name == 'start') {
            transit.search(v, start_destination);
        } else {
            transit.search(start_destination, v);
        }
        re_color();
        transit1.style.color = 'red';
        result1.style.display = 'none';
        result2.style.display = 'block';
        map.setZoom(14);
        map.setCenter(start_destination);
    } else {
        alert(`请输入{class_name=='start'?'起点':'终点'}！😊`);
    }
}

transit1.onclick = function() {
    let v = keyword.value;
    let class_name = s_or_e.className;
    if (v != '') {
        driving.clearResults();
        transit.clearResults();
        re_color();
        if (class_name == 'start') {
            transit.search(v, start_destination);
        } else {
            transit.search(start_destination, v);
        }
        transit1.style.color = 'red';
        result1.style.display = 'none';
        result2.style.display = 'block';
        map.setZoom(14);
        map.setCenter(start_destination);
    } else {
        alert(`请输入{class_name=='start'?'起点':'终点'}！😊`);
    }
}

driving1.onclick = function() {
    let v = keyword.value;
    let class_name = s_or_e.className;
    if (v != '') {
        driving.clearResults();
        transit.clearResults();
        if (class_name == 'start') {
            driving.search(v, start_destination);
        } else {
            driving.search(start_destination, v);
        }
        re_color();
        driving1.style.color = 'red';
        result1.style.display = 'block';
        result2.style.display = 'none';
        map.setZoom(8);
    } else {
        alert(`请输入{class_name=='start'?'起点':'终点'}！😊`);
    }
}

var trans_btn = document.querySelector(".transformation");
// 转换按钮
trans_btn.onclick = function() {
    if (s_or_e.className == 'start') {
        s_or_e.innerText = '终点：'
        s_or_e.className = 'end';
    } else if (s_or_e.className == 'end') {
        s_or_e.innerText = '起点：';
        s_or_e.className = 'start';
    }
}
